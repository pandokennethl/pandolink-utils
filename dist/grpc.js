"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setRequest = void 0;
const setRequest = (request, params) => {
    const paramKeys = Object.keys(params);
    for (let c = 0; c < paramKeys.length; c++) {
        const key = paramKeys[c];
        const funcName = `set${key.charAt(0).toUpperCase() + key.slice(1)}`;
        // @ts-ignore
        request[funcName](params[key]);
    }
    return request;
};
exports.setRequest = setRequest;
//# sourceMappingURL=grpc.js.map